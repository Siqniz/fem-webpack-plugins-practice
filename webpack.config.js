const path = require('path');

//plugins
const htmlWebpackPlugin = require('html-webpack-plugin');
const miniExtractCssPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry:'./src/index.js',
    output:{
        filename:"bundle.js",
        path: path.join(__dirname,'./dist')
    },
    plugins:[
        new htmlWebpackPlugin({
            title:'Webpack Plugins',
            template:'./src/index.html'
        }),
        new miniExtractCssPlugin()
    ],
    module: {
    rules:[
            {
                test:/\.scss$/, 
                use:[miniExtractCssPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test:/\.jpeg/,
                use:[
                    {
                        loader:'file-loader',
                        options:{
                            name:'[name].[ext]',
                            outputPath:'imgs/'
                        }
                    }
                ]

            }
        ]
    }
}